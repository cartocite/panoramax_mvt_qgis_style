# Projet QGIS de style pour la couche de tuiles vectorielles de Panoramax

Projet QGIS imitant le rendu Panoramax mettant en avant la date des photos. Il est possible de modifier le style de la couche pour modifier les périodes. Les périodes sont exprimées en jours dans les expressions des filtres des catégories de la symbologie.
